using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour 
{
    [SerializeField] private string _gameVersion;
    [SerializeField] private byte _maxPlayersCount;

    public string GameVersion => _gameVersion;
    public byte MaxPlayerCount => _maxPlayersCount;
}
