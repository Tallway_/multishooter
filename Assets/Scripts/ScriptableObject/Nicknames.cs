using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "Nicknames", menuName = "MultiShooter/Nicknames", order = 0)]
public class Nicknames : ScriptableObject 
{
    [SerializeField] private List<string> _nicknames;

    public IEnumerable<string> ListOfExistingNicknames => _nicknames;
}
