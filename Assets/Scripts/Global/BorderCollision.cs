﻿using System;
using MultiShooter.Bullets;
using UnityEngine;

namespace MultiShooter.Global
{
    [RequireComponent(typeof(Rigidbody), typeof(Collider))]
    public class BorderCollision : MonoBehaviour
    {
        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.TryGetComponent(out Bullet bullet))
            {
                Destroy(bullet.gameObject);
            }
        }
    }
}