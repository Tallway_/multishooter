﻿namespace MultiShooter.Global
{
    public class Timer
    {
        public bool IsFinished { get; private set; }
        public float Progress { get; private set; }

        private float _timeValue;
        private float _currentTimeValue;

        public Timer()
        {
            _timeValue = 0f;
            _currentTimeValue = 0f;
            Progress = 0f;
            IsFinished = true; 
        }
        
        public Timer(float time)
        {
            _timeValue = time;
            _currentTimeValue = time;
            Progress = 0f;
            IsFinished = true;
        }

        public void Tick(float delta)
        {
            if (IsFinished) return;
            
            _currentTimeValue -= delta;
            Progress = (_timeValue - _currentTimeValue) / _timeValue;

            if (_currentTimeValue <= 0f)
            {
                IsFinished = true;
            }
        }

        public void Reset()
        {
            _currentTimeValue = _timeValue;
            IsFinished = false;
        }

        public void SetTimeValue(float time)
        {
            _timeValue = time;
        }
    }
}