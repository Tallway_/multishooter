using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using System.Linq;
using MultiShooter.Sync;

namespace MultiShooter.Global
{
    public class Connection : MonoBehaviourPunCallbacks
    {
        [SerializeField] private NicknameBuilder _nicknameBuilder;
        [SerializeField] private GameSettings _gameSettings;

        private bool _isConnecting;

        private void Awake() 
        {
            PhotonNetwork.AutomaticallySyncScene = true;
            
            CustomTypes.Register();
        }

        void Start()
        {
            PhotonNetwork.IsMessageQueueRunning = true;
        }

        public void Connect()
        {
            PhotonNetwork.NickName = _nicknameBuilder.GetRandomNicknameExceptExisting(PhotonNetwork.PlayerList.ToList());
            PhotonNetwork.GameVersion = _gameSettings.GameVersion;

            if(PhotonNetwork.IsConnected)
            {
                Join();
            }
            else
            {
                _isConnecting = PhotonNetwork.ConnectUsingSettings();
                PhotonNetwork.GameVersion = _gameSettings.GameVersion;
            }
        }

        void Join()
        {
            PhotonNetwork.JoinOrCreateRoom("TEST", new RoomOptions()
            {
                MaxPlayers = _gameSettings.MaxPlayerCount,
                DeleteNullProperties = true,
                PublishUserId = true
            }, null);
        }

        public override void OnConnectedToMaster()
        {
            if(_isConnecting)
            {
                Join();
                
                _isConnecting = false;
            }

            Debug.Log("Connected to server.");
            Debug.LogFormat("Connected player - {0}", PhotonNetwork.LocalPlayer.NickName);
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            Debug.LogWarningFormat("Disconnected with reason - {0}", cause);

            _nicknameBuilder.ReturnNickname(PhotonNetwork.NickName);
            _isConnecting = false;
        }

//        public override void OnJoinRandomFailed(short returnCode, string message)
//        {
//            Debug.Log("Join room failed. Creating a room ...");
//
//            RoomOptions roomOptions = new RoomOptions
//            {
//                MaxPlayers = _gameSettings.MaxPlayerCount
//            };
//
//            PhotonNetwork.CreateRoom("Game Room", roomOptions);
//        }

        public override void OnJoinedRoom()
        {
            if(PhotonNetwork.CurrentRoom.PlayerCount == 1)
            {
                PhotonNetwork.LoadLevel(1);
            }
        }
    }
}