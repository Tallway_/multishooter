﻿namespace IcarosSDK.Utilities
{
    public interface IInitilization
    {
        void Init();
    }
}