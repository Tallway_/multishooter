using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace MultiShooter.Global
{
    public class NicknameBuilder : MonoBehaviour 
    {
        [SerializeField] private Nicknames _nicknames;

        private List<string> _freeNicknames = new List<string>();
        private int count;

        private const string Underscore = "_";

        public void Start()
        {
            _freeNicknames.Clear();

            FillFreeNicknames(null);
        }

        public string GetRandomNicknameExceptExisting(List<Player> _players)
        {
            if(_players.Count == 0)
            {
                return GetRandomNickname();
            }

            foreach(Player player in _players)
            {
                string removingFreeNickname = _freeNicknames.Find(nickname => player.NickName == nickname);

                _freeNicknames.Remove(removingFreeNickname);
            }

            return GetRandomNickname();
        }

        public void ReturnNickname(string nickname)
        {
            _freeNicknames.Add(nickname);
        }

        private string GetRandomNickname()
        {
            if (_freeNicknames.Count == 0)
            {
                count++;
                FillFreeNicknames(Underscore + count);
            }

            int randomIndex = Random.Range(0, _freeNicknames.Count);
            string randomNickname = _freeNicknames[randomIndex];

            _freeNicknames.Remove(randomNickname);

            return randomNickname;
        }

        private void FillFreeNicknames(string postfix)
        {
            foreach (string nickname in _nicknames.ListOfExistingNicknames)
            {
                _freeNicknames.Add(nickname + postfix);
            }
        }
    }
}