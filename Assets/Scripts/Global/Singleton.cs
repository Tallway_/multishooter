﻿using UnityEngine;

namespace IcarosSDK.Utilities
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                {
                    GameObject singleton = new GameObject();
                    
                    singleton.AddComponent<T>();
                    
                    Init(singleton);
                }
            
                return _instance;
            }
        }

        public void Remove()
        {
            _instance = null;
            Destroy(this);
        }

        public static bool IsInstancePersist()
        {
            return _instance != null;
        }

        static void Init(GameObject singleton)
        {
            _instance = singleton.GetComponent<T>();
            
            singleton.name = "(singleton) " + typeof(T);

            if (_instance is IInitilization)
            {
                (_instance as IInitilization).Init();
            }

            DontDestroyOnLoad(singleton);
        }

        public void Awake()
        {
            if (_instance == null)
            {
                Init(gameObject);
            }
        }
    }
}