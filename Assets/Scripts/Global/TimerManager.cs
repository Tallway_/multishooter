﻿using System;
using System.Collections;
using IcarosSDK.Utilities;
using UnityEngine;

namespace IcarosSDK.Scripts.Utilities
{
    public class TimerManager : Singleton<TimerManager>
    {
        public void Invoke(Action action, float time, bool isRealTime = true)
        {   
            StartCoroutine(InvokeAfterTime(action, time, isRealTime));
        }

        IEnumerator InvokeAfterTime(Action action, float time, bool isRealTime = true)
        {
            if (isRealTime)
            {
                yield return new WaitForSecondsRealtime(time);
            }
            else
            {
                yield return new WaitForSeconds(time);
            }
            action();
        }

        public void Repeat(GameObject go, Action action, float time)
        {
            StartCoroutine(RepeatAfterTime(go, action, time));
        }

        public void Repeat(Func<bool> untilCallback, float time)
        {
            StartCoroutine(RepeatUntil(untilCallback, time));
        }
        
        IEnumerator RepeatAfterTime(GameObject go, Action action, float time)
        {
            while (go != null && go.activeSelf)
            {
                yield return new WaitForSecondsRealtime(time);
                
                if (go != null && go.activeSelf)
                {
                    action();
                }
            }
        }
        
        IEnumerator RepeatUntil(Func<bool> untilCallback, float time)
        {
            while (untilCallback())
            {
                yield return new WaitForSecondsRealtime(time);
            }
        }
    }
}