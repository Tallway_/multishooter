﻿using System;
using System.Collections;
using System.Collections.Generic;
using MultiShooter.Enemy;
using MultiShooter.PlayerBehavior;
using MultiShooter.Sync;
using Photon.Pun;
using UnityEngine;
using Random = UnityEngine.Random;

namespace MultiShooter.Global
{
    [RequireComponent(typeof(PhotonView))]
    public class EnemySpawner : MonoBehaviour
    {
        public int RandomPlayerViewID { get; private set; }
        
        [SerializeField] private EnemyMovement _enemyPrefab;
        [SerializeField] private float _spawnRate;

        [SerializeField] private int _defaultNumbers = 2;
        
        private SyncVar<int> _syncVarNumbers;

        [Header("----Bounds----")]
        [SerializeField] private float _xBottomBound;
        [SerializeField] private float _xTopBound;
        [SerializeField] private float _zBottomBound;
        [SerializeField] private float _zTopBound;
        
        private PhotonView _photonView;
        private List<PlayerBehaviorContainer> _roomPlayers;
        private SyncVar<int> _syncPlayerViewID;
        private Timer _timer;
        
        private const float YSpawnPosition = 1f;
        private void Awake() 
        {
            _photonView = GetComponent<PhotonView>();
            _timer = new Timer(_spawnRate);
            _timer.Reset();
            
            _syncVarNumbers = new SyncVar<int>("_syncVarNumbers", _photonView);

            if (PhotonNetwork.IsMasterClient)
            {
                _syncVarNumbers.SetProperty(_defaultNumbers);
            }
        }
        
        private void Start()
        {
            _roomPlayers = GameManager.Instance.RoomPlayers;
            
            _syncPlayerViewID = new SyncVar<int>(nameof(RandomPlayerViewID), _photonView);
            
            if (!_photonView.IsMine)
            {
                return;
            }
        }

        private void Update()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                if(_timer.IsFinished)
                {
                    _timer.Reset();
                    
                    if (_syncVarNumbers.GetProperty() <= 0)
                    {
                        return;
                    }
                    
                    Spawn();
                }
                else
                {
                    _timer.Tick(Time.deltaTime);
                }
            }
            else
            {
                RandomPlayerViewID = _syncPlayerViewID.GetProperty();
            }
        }

        private void Spawn()
        {   
            Vector3 randomPosition = Vector3.zero;
            randomPosition.x = Random.Range(_xBottomBound, _xTopBound);
            randomPosition.y = YSpawnPosition;
            randomPosition.z = Random.Range(_zBottomBound, _zTopBound);

            _syncVarNumbers.SetProperty(_syncVarNumbers.GetProperty() - 1);

            GameObject enemy = PhotonNetwork.InstantiateRoomObject(_enemyPrefab.name, randomPosition, Quaternion.identity);
        }
    }
}