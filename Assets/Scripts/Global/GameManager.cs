using System;
using System.Collections.Generic;
using MultiShooter.PlayerBehavior;
using MultiShooter.Sync;
using MultiShooter.UI;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MultiShooter.Global
{
    public class GameManager : MonoBehaviourPunCallbacks 
    {
        public static GameManager Instance { get; private set; }
        public List<PlayerBehaviorContainer> RoomPlayers { get; set; }

        [SerializeField] private ShooterGUI _gui;
        [SerializeField] private GameObject _playerPrefab;

        private void Awake() 
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                Instance = this;
            }
            
            RoomPlayers = new List<PlayerBehaviorContainer>();
        }

        public PlayerBehaviorContainer GetPlayer(int viewId)
        {
            foreach (PlayerBehaviorContainer player in RoomPlayers)
            {
                if (player != null && player.PhotonView.ViewID == viewId)
                {
                    return player;
                }
            }

            return null;
        }


        private void Start()
        {
            float randomXPosition = UnityEngine.Random.Range(-20f, 20f);
            float randomYPosition = UnityEngine.Random.Range(4f, 5f);
            float randomZPosition = UnityEngine.Random.Range(-20f, 20f);
            Vector3 spawnPosition = new Vector3(randomXPosition, randomYPosition, randomZPosition);

            if (PlayerController.LocalPlayerInstance != null) return;
            
            GameObject newPlayer = PhotonNetwork.Instantiate(
                _playerPrefab.name, 
                spawnPosition, 
                Quaternion.identity, 
                0);
            
            _gui.SetTrackingPlayer(newPlayer);
        }

        public override void OnLeftRoom()
        {
            PhotonNetwork.IsMessageQueueRunning = false;
            
            SceneManager.LoadScene(0);
        }

        public void LeaveRoom()
        {
            if (PhotonNetwork.IsConnectedAndReady)
            {
                Invoke("LeaveAfterTime", 1f);
            }
        }

        public void LeaveAfterTime()
        {
            PhotonNetwork.LeaveRoom();
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            base.OnPlayerLeftRoom(otherPlayer);
        }
    }
}