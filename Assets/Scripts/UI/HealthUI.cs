﻿using System;
using MultiShooter.PlayerBehavior;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MultiShooter.UI
{
    public class HealthUI : MonoBehaviour
    {
        [SerializeField] private Image _healthFillingImage;
        [SerializeField] private TextMeshProUGUI _healthText;
        [SerializeField] private float _lerpSpeed;

        private ShooterGUI _gui;
        private PlayerHealth _playerHealth;

        private bool _isPlayerAttached;
        private float _maxHealth, _currentHealth;

        private void Awake()
        {
            _gui = GetComponentInParent<ShooterGUI>();
        }

        private void OnEnable()
        {
            _gui.OnPlayerAttached += OnPlayerAttached;
        }

        private void OnDisable()
        {
            _gui.OnPlayerAttached -= OnPlayerAttached;
        }

        private void OnPlayerAttached(GameObject playerObject)
        {
            _playerHealth = playerObject.GetComponent<PlayerBehaviorContainer>().PlayerHealth;
            _maxHealth = _playerHealth.Health;
            _currentHealth = _maxHealth;
            _isPlayerAttached = true;
        }

        private void Update()
        {
            _currentHealth = Mathf.Lerp(_currentHealth, _playerHealth.Health, Time.deltaTime * _lerpSpeed);

            int roundedValue = Mathf.RoundToInt(_currentHealth);
            _healthFillingImage.fillAmount = roundedValue / _maxHealth;
            _healthText.text = roundedValue.ToString();
        }
    }
}