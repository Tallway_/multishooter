﻿using System;
using MultiShooter.PlayerBehavior;
using TMPro;
using UnityEngine;

namespace MultiShooter.UI
{
    public class NickNameUI : MonoBehaviour
    {
        private TextMeshProUGUI _nickName;
        private ShooterGUI _gui;

        private void Awake()
        {
            _nickName = GetComponent<TextMeshProUGUI>();
            _gui = GetComponentInParent<ShooterGUI>();
        }

        private void OnEnable()
        {
            _gui.OnPlayerAttached += OnPlayerAttached;
        }

        private void OnDisable()
        {
            _gui.OnPlayerAttached -= OnPlayerAttached;
        }

        private void OnPlayerAttached(GameObject playerObject)
        {
            _nickName.text = playerObject.GetComponent<PlayerBehaviorContainer>().PlayerNickName.LocalNickName;
        }
    }
}