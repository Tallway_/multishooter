using System;
using MultiShooter.Global;
using UnityEngine;

namespace MultiShooter.UI
{
    public class ShooterGUI : MonoBehaviour
    {
        public event Action<GameObject> OnPlayerAttached;

        public void SetTrackingPlayer(GameObject playerObject)
        {
            OnPlayerAttached?.Invoke(playerObject);
        }
    }
}