﻿using System;
using MultiShooter.Global;
using MultiShooter.PlayerBehavior;
using Photon.Pun;
using UnityEngine;

namespace MultiShooter.Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField, Range(0, 100)] private float _damage;

        private EnemyHealth _health;
        private PhotonView _photonView;
        private bool appliedDamage;

        private void Awake()
        {
            _health = GetComponent<EnemyHealth>();
            _photonView = PhotonView.Get(gameObject);
        }

        private void OnEnable()
        {
            _health.OnEnemyDead += DestroyEnemy;
        }

        private void OnDestroy()
        {
            _health.OnEnemyDead -= DestroyEnemy;
        }

        void DestroyEnemy()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                PhotonNetwork.Destroy(gameObject);
            }
        }

        [PunRPC]
        private void ApplyDamage(int playerViewId, float amount)
        {
            if (PhotonNetwork.IsMasterClient && !appliedDamage)
            {
                appliedDamage = true;
                
                PlayerBehaviorContainer player = GameManager.Instance.GetPlayer(playerViewId);

                if (player != null)
                {
                    player.PlayerHealth.ApplyDamage(amount);
                }
                else
                {
                    Debug.LogError("Player not found");
                }
                
                DestroyEnemy();
            }
        }
        
        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.TryGetComponent(out PlayerBehaviorContainer _player))
            {
                _photonView.RPC(nameof(ApplyDamage), RpcTarget.MasterClient, _player.PhotonView.ViewID, _damage);
            }
        }
    }
}