﻿using System;
using Photon.Pun;
using UnityEngine;

namespace MultiShooter.Enemy
{
    public class EnemyHealth : MonoBehaviour
    {
        public event Action OnEnemyDead;
        
        [SerializeField] private float _startHealth;

        private PhotonView _photonView;
        private float _currentHealth;

        private void Awake()
        {
            _photonView = PhotonView.Get(gameObject);
            _currentHealth = _startHealth;
        }

        public bool ApplyDamage(float damageValue)
        {
            if (!_photonView.IsMine) return false;
            
            if(damageValue < 0f)
            {
                Debug.Log("(EnemyHealth) Incorrect damage value. Damage must be >= 0.");
                return false;
            }

            float newHealthValue = _currentHealth - damageValue;

            if(newHealthValue <= 0f)
            {
                _currentHealth = 0f;
                
                OnEnemyDead?.Invoke();
                return true;
            }

            _currentHealth = newHealthValue;

            return true;
        }
    }
}