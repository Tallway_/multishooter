﻿using System;
using System.Collections;
using System.Collections.Generic;
using MultiShooter.Global;
using MultiShooter.PlayerBehavior;
using MultiShooter.Sync;
using Photon.Pun;
using Photon.Pun.Demo.Cockpit;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UIElements.Experimental;
using Random = UnityEngine.Random;

namespace MultiShooter.Enemy
{
    [RequireComponent(typeof(PhotonView))]
    public class EnemyMovement : MonoBehaviourPunCallbacks
    {
        [SerializeField] private float _speed;

        private EnemySpawner _enemySpawner;
        private PhotonView _photonView;

        private PlayerBehaviorContainer _target;

        private SyncVar<Vector3> _syncCurrentPosition;

        private SyncVar<int> _syncEnemyTargetViewId;

        public bool isDestroyed = false;

        private void Awake()
        {
            _enemySpawner = FindObjectOfType<EnemySpawner>();
            _photonView = PhotonView.Get(gameObject);
            
            InitCurrentPosition();
            InitEnemyTargetViewId();
        }

        void InitCurrentPosition()
        {
            _syncCurrentPosition = new SyncVar<Vector3>("_syncCurrentPosition", _photonView, () =>
            {
                return transform.position;
            });
            
            _syncCurrentPosition.InitValue((value) =>
            {
                if (!isDestroyed)
                {
                    transform.position = value;
                }
            });
        }
        
        void InitEnemyTargetViewId()
        {
            _syncEnemyTargetViewId = new SyncVar<int>("_syncEnemyTargetViewId", _photonView);
        }
                
        private void Update()
        {
            UpdateTarget();
            MoveToTarget();
        }

        void MoveToTarget()
        {
            if (_target == null)
            {
                return;
            }
            
            transform.position += (_target.transform.position - transform.position).normalized * Time.deltaTime * _speed;
        }

        void UpdateTarget()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                _target = TakeNearestPlayer();

                if (_target != null && _syncEnemyTargetViewId.GetProperty() != _target.PhotonView.ViewID)
                {
                    _syncEnemyTargetViewId.SetProperty(_target.PhotonView.ViewID);
                }
            }
            else
            {
                if (_target == null || _target.PhotonView.ViewID != _syncEnemyTargetViewId.GetProperty() )
                {
                    _target = GameManager.Instance.GetPlayer(_syncEnemyTargetViewId.GetProperty());
                }
            }
        }
        
        private PlayerBehaviorContainer TakeNearestPlayer()
        {
            PlayerBehaviorContainer nearestPlayer = null;
            float nearestDistance = 0f;

            foreach (PlayerBehaviorContainer player in GameManager.Instance.RoomPlayers)
            {
                if (player != null)
                {
                    float distance = Vector3.Distance(transform.position, player.gameObject.transform.position);

                    if (nearestDistance == 0f)
                    {
                        nearestDistance = distance;
                        nearestPlayer = player;

                        continue;
                    }

                    if (distance < nearestDistance)
                    {
                        nearestDistance = distance;
                        nearestPlayer = player;
                    }
                }
            }

            return nearestPlayer;
        }

        private void OnDestroy()
        {
            _syncCurrentPosition.RemoveProperty();
            _syncEnemyTargetViewId.RemoveProperty();

            isDestroyed = true;
        }
    }
}