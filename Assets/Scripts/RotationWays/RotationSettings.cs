using System;
using UnityEngine;

namespace MultiShooter.RotationWays
{
    [Serializable]
    public class RotationSettings
    {
        [SerializeField] private float _sensitivityHorizontal = 9.0f;
        [SerializeField] private float _sensitivityVertical = 9.0f;
        [SerializeField] private float _minimumVerticalAngle = -45.0f;
        [SerializeField] private float _maximumVerticalAngle = 45.0f;
        [SerializeField] private float _minimumHorizontalAngle = -45.0f;
        [SerializeField] private float _maximumHorizontalAngle = 45.0f;
        [SerializeField] private float _lerpSpeed = 10f;

        public float SensitivityHorizontal => _sensitivityHorizontal; 
        public float SensitivityVertical => _sensitivityVertical; 
        public float MinimumVerticalAngle => _minimumVerticalAngle; 
        public float MaximumVerticalAngle => _maximumVerticalAngle; 
        public float MinimumHorizontalAngle  => _minimumHorizontalAngle;
        public float MaximumHorizontalAngle => _maximumHorizontalAngle;
        public float LerpSpeed => _lerpSpeed; 
    }
}