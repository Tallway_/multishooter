using UnityEngine;

namespace MultiShooter.RotationWays
{
    public class VerticalRotation : RotationWay
    {
        private float _rotationY = 0f;

        public override Quaternion Tick(float delta)
        {
            _rotationY += PlayerInput.MouseY * RotationSettings.SensitivityVertical;

            _rotationY = Mathf.Clamp(_rotationY, RotationSettings.MinimumVerticalAngle, RotationSettings.MaximumVerticalAngle);

            Quaternion yRotation = Quaternion.AngleAxis(_rotationY, -Vector3.right);

            Quaternion newRotation = Quaternion.Lerp(
                RotatedObject.transform.localRotation,
                StartRotation * yRotation,
                delta * RotationSettings.LerpSpeed);

            return newRotation;
        }
    }
}