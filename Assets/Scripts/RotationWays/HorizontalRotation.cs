using UnityEngine;

namespace MultiShooter.RotationWays
{
    public class HorizontalRotation : RotationWay
    {
        private float _rotationX = 0f;

        public override Quaternion Tick(float delta)
        {
            _rotationX += PlayerInput.MouseX * RotationSettings.SensitivityHorizontal;

            _rotationX = Mathf.Clamp(_rotationX, RotationSettings.MinimumHorizontalAngle, RotationSettings.MaximumHorizontalAngle);

            Quaternion xRotation = Quaternion.AngleAxis(_rotationX, Vector3.up);

            Quaternion newRotation = Quaternion.Lerp(
                RotatedObject.transform.localRotation,
                StartRotation * xRotation,
                delta * RotationSettings.LerpSpeed);

            return newRotation;
        }
    }
}