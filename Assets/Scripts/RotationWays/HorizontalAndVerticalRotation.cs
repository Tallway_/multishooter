using MultiShooter.PlayerBehavior;
using UnityEngine;

namespace MultiShooter.RotationWays
{
    public class HorizontalAndVerticalRotation : RotationWay
    {
        private float _rotationX = 0f;
        private float _rotationY = 0f;

        public override Quaternion Tick(float delta)
        {
            _rotationX += PlayerInput.MouseX * RotationSettings.SensitivityHorizontal;
            _rotationY += PlayerInput.MouseY * RotationSettings.SensitivityVertical;

            _rotationX = Mathf.Clamp(_rotationX, RotationSettings.MinimumHorizontalAngle, RotationSettings.MaximumHorizontalAngle);
            _rotationY = Mathf.Clamp(_rotationY, RotationSettings.MinimumVerticalAngle, RotationSettings.MaximumVerticalAngle);

            Quaternion xRotation = Quaternion.AngleAxis(_rotationX, Vector3.up);
            Quaternion yRotation = Quaternion.AngleAxis(_rotationY, -Vector3.right);

            Quaternion newRotation = Quaternion.Lerp(
                RotatedObject.transform.localRotation,
                StartRotation * xRotation * yRotation,
                delta * RotationSettings.LerpSpeed);

            return newRotation;
        }
    }
}