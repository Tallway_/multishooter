using MultiShooter.PlayerBehavior;
using UnityEngine;

namespace MultiShooter.RotationWays
{
    public abstract class RotationWay : MonoBehaviour
    {
        protected GameObject RotatedObject;
        protected RotationSettings RotationSettings;
        protected PlayerInput PlayerInput;
        protected Quaternion StartRotation;

        public void Init(GameObject rotatedObject, PlayerInput playerInput, RotationSettings rotationSettings, Quaternion startRotation)
        {
            RotatedObject = rotatedObject;
            PlayerInput = playerInput;
            RotationSettings = rotationSettings;
            StartRotation = startRotation;
        }

        public abstract Quaternion Tick(float delta);
    }
}