﻿using System;
using MultiShooter.PlayerBehavior;
using UnityEngine;

namespace MultiShooter.Bullets
{
    [RequireComponent(typeof(Rigidbody), typeof(Collider))]
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float _speed;

        private Rigidbody _rigidbody;
        private PlayerGun _playerGun;
        private Vector3 _direction;
        
        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public void Init(Vector3 direction)
        {
            _direction = direction;
        }

        public void Launch()
        {
            SetVelocity();
        }
        
        private void SetVelocity()
        {
            _rigidbody.velocity = _direction * _speed;
        }
    }
}