﻿using System;
using MultiShooter.Enemy;
using UnityEngine;

namespace MultiShooter.Bullets
{
    public class BulletTakingDamage : MonoBehaviour
    {
        [SerializeField] private float _damage;

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.TryGetComponent(out EnemyHealth _health))
            {
                _health.ApplyDamage(_damage);
                
                Destroy(gameObject);
            }
        }
    }
}