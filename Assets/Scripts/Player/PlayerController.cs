using System;
using MultiShooter.Global;
using Photon.Pun;
using UnityEngine;

namespace MultiShooter.PlayerBehavior
{
    [RequireComponent(typeof(PlayerBehaviorContainer))]
    public class PlayerController : MonoBehaviourPun, IPunObservable
    {
        public static GameObject LocalPlayerInstance;

        private PlayerBehaviorContainer _playerBehaviorContainer;
        private PlayerHealth _playerHealth;
        private PhotonView _photonView;

        private Vector3 currentPosition;
        private Quaternion currentRotation;

        protected float speedTransformSync = 10f;

        public bool isInitPos;
        
        private void Awake() 
        {
            _playerBehaviorContainer = GetComponent<PlayerBehaviorContainer>();
            _playerHealth = _playerBehaviorContainer.PlayerHealth;
            _photonView = _playerBehaviorContainer.PhotonView; 

            if(_photonView.IsMine)
            {
                LocalPlayerInstance = gameObject;
            }
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(transform.position);
                stream.SendNext(transform.rotation);
            }
            else
            {
                currentPosition = (Vector3)stream.ReceiveNext();
                currentRotation = (Quaternion)stream.ReceiveNext();

                if (!isInitPos)
                {
                    transform.position = currentPosition;
                    transform.rotation = currentRotation;

                    isInitPos = true;
                }
            }
        }

        void Update()
        {
            if (!photonView.IsMine && isInitPos)
            {
                transform.position = Vector3.Lerp(transform.position, currentPosition, Time.deltaTime * speedTransformSync);
                transform.rotation = Quaternion.Lerp(transform.rotation, currentRotation, Time.deltaTime * speedTransformSync);
            }
        }

        private void OnEnable()
        {
            GameManager.Instance.RoomPlayers.Add(_playerBehaviorContainer);
        }

        private void OnDisable()
        {
            GameManager.Instance.RoomPlayers.Remove(_playerBehaviorContainer);
        }

        private void Start()
        {
            _playerHealth.OnPlayerDead += OnPlayerDead;
        }

        private void OnDestroy() 
        {
            _playerHealth.OnPlayerDead -= OnPlayerDead;

            if (_photonView != null && _photonView.IsMine && PhotonNetwork.CurrentRoom != null)
            {
                // PhotonNetwork.CurrentRoom.SetCustomProperties(new Hashtable() {{$"[{_photonView.ViewID}].Color", null}});
            }
        }

        private void OnPlayerDead()
        {
            GameManager.Instance.LeaveRoom();
            
            PhotonNetwork.Destroy(gameObject);
        }
    }
}