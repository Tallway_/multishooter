using Photon.Pun;
using UnityEngine;

namespace MultiShooter.PlayerBehavior
{
    public class PlayerCollisionHandler : MonoBehaviour
    {
        private PlayerBehaviorContainer _playerBehaviorContainer;
        private PlayerHealth _playerHealth;
        private PhotonView _photonView;

        private void Awake() 
        {
            _playerBehaviorContainer = GetComponentInParent<PlayerBehaviorContainer>();
            _playerHealth = _playerBehaviorContainer.PlayerHealth;
            _photonView = _playerBehaviorContainer.PhotonView;
        }

        private void OnCollisionEnter(Collision other) 
        {
            if (!_photonView.IsMine)
            {
                return;
            }
        }
    }
}