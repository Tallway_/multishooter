using System;
using MultiShooter.RotationWays;
using Photon.Pun;
using UnityEngine;

namespace MultiShooter.PlayerBehavior
{
    public class RotationByMouse : MonoBehaviour 
    {
        // [SerializeField] private RotationWay _rotationWayPrefab;
        //[SerializeField] private RotationSettings _rotationSettings;
        [SerializeField] private GameObject _rotatedObject;

        private PhotonView _photonView;
        private PlayerInput _playerInput;
        private Quaternion _originalRotation;
        private Camera _camera;

        private void Awake() 
        {
            _photonView = GetComponentInParent<PhotonView>();
            _playerInput = GetComponentInParent<PlayerBehaviorContainer>().PlayerInput;
        }

        private void Start()
        {
            _camera = Camera.main;
        }

        private void Update()
        {
            if (!_photonView.IsMine)
            {
                return;
            }

            Rotate();
        }

        private void Rotate()
        {
            Vector3 currentPosition = _rotatedObject.transform.position;
            Vector3 targetPosition = CalculateTargetPosition();;

            targetPosition.y = currentPosition.y;
            
            _rotatedObject.transform.LookAt(targetPosition);
        }

        private Vector3 CalculateTargetPosition()
        {
            Vector3 mouseScreenPosition = _playerInput.MouseScreenPosition;

            Ray ray = _camera.ScreenPointToRay(mouseScreenPosition);
            RaycastHit hit;
            Vector3 targetPosition = Vector3.zero;

            if (Physics.Raycast(ray, out hit, float.PositiveInfinity, LayerMask.GetMask("Floor")))
            {
                targetPosition = hit.point;
            }

            return targetPosition;
        }
    }
}