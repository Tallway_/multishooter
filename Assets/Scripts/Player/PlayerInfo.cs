using System;
using MultiShooter.PlayerBehavior;
using UnityEngine;
using UnityEngine.UI;

namespace MultiShooter.PlayerBehavior
{
    public class PlayerInfo : MonoBehaviour 
    {
        private PlayerBehaviorContainer _playerBehaviorContainer;
        private PlayerNickName _playerNickName;
        private PlayerHealth _playerHealth;

        private void Awake()
        {
            _playerBehaviorContainer = GetComponentInParent<PlayerBehaviorContainer>();
            _playerNickName = _playerBehaviorContainer.PlayerNickName;
            _playerHealth = _playerBehaviorContainer.PlayerHealth;
        }
    }
}
