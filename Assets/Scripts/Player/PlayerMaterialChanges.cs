using ExitGames.Client.Photon;
using MultiShooter.Sync;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace MultiShooter.PlayerBehavior
{
    [RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
    public class PlayerMaterialChanges : MonoBehaviour 
    {
        private MeshRenderer _meshRenderer;
        private PlayerBehaviorContainer _playerBehaviourContainer;
        private PhotonView _photonView;

        public SyncVar<Color> MaterialColor { get; private set; }

        public Color PlayerMaterialColor
        {
            get => _meshRenderer.material.color;
            set => _meshRenderer.material.color = value;
        }

        private void Awake() 
        {
            _meshRenderer = GetComponent<MeshRenderer>();
            _playerBehaviourContainer = GetComponentInParent<PlayerBehaviorContainer>();
            _photonView = _playerBehaviourContainer.PhotonView;

            MaterialColor = new SyncVar<Color>(nameof(MaterialColor), _photonView);
        }

        private void Start()
        {
            if(!_photonView.IsMine)
            {
                return;
            }
            
            var randomRedChanel = Random.Range(0f, 1f);
            var randomGreenChanel = Random.Range(0f, 1f);
            var randomBlueChanel = Random.Range(0f, 1f);

            PlayerMaterialColor = new Color(randomRedChanel,
                                            randomGreenChanel,
                                            randomBlueChanel);
        }

        private void Update()
        {
            if (_photonView.IsMine)
            {
                MaterialColor.SetProperty(PlayerMaterialColor);
                return;
            }

            PlayerMaterialColor = MaterialColor.GetProperty();
        }
    }
}