using Photon.Pun;
using UnityEngine;

namespace MultiShooter.PlayerBehavior
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private float _speed;

        private PlayerBehaviorContainer _playerBehaviorContainer;
        private PlayerInput _playerInput;
        private PhotonView _photonView;

        private void Awake()
        {
            _playerBehaviorContainer = GetComponentInParent<PlayerBehaviorContainer>();
            _playerInput = _playerBehaviorContainer.PlayerInput;
            _photonView = _playerBehaviorContainer.PhotonView;
        }

        private void Update() 
        {
            if(!_photonView.IsMine)
            {
                return; 
            }

            Vector3 newPosition = Vector3.zero;

            newPosition.x = _playerInput.HorizontalDirection;
            newPosition.z = _playerInput.VerticalDirection;

            //newPosition = transform.TransformDirection(newPosition) * _speed * Time.deltaTime;
            newPosition = newPosition * _speed * Time.deltaTime;
            
            _playerBehaviorContainer.transform.position += newPosition;
        }
    }
}