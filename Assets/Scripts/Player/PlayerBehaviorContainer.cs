using Photon.Pun;
using UnityEngine;

namespace MultiShooter.PlayerBehavior
{
    public class PlayerBehaviorContainer : MonoBehaviour 
    {
        [SerializeField] private PlayerInput _playerInput;
        [SerializeField] private PlayerMovement _playerMovement;
        [SerializeField] private PlayerCollisionHandler _playerCollisionHandler;
        [SerializeField] private PlayerHealth _playerHealth;
        [SerializeField] private PlayerMaterialChanges _playerMaterialChanges;
        [SerializeField] private PlayerController _playerController;
        [SerializeField] private PlayerNickName _playerNickName;
        [SerializeField] private PhotonView _photonView;

        public PlayerInput PlayerInput => _playerInput;
        public PlayerMovement PlayerMovement => _playerMovement;
        public PlayerCollisionHandler PlayerCollisionHandler => _playerCollisionHandler;
        public PlayerHealth PlayerHealth => _playerHealth;
        public PlayerMaterialChanges PlayerMaterialChanges => _playerMaterialChanges;
        public PlayerController PlayerController => _playerController;
        public PlayerNickName PlayerNickName => _playerNickName;
        public PhotonView PhotonView => _photonView;
    }
}