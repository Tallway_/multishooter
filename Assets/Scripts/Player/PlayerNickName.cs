using MultiShooter.Sync;
using Photon.Pun;
using UnityEngine;

namespace MultiShooter.PlayerBehavior
{
    public class PlayerNickName : MonoBehaviour
    {
        [SerializeField] private string _localNickName;
        
        public string LocalNickName
        {
            get => _localNickName;
            set
            {
                _localNickName = value;
                _container.gameObject.name = value;
            }
        }
        
        private PlayerBehaviorContainer _container;
        private PhotonView _photonView;

        public SyncVar<string> NickName { get; private set; }

        private void Awake() 
        {
            _container = GetComponentInParent<PlayerBehaviorContainer>();
            _photonView = _container.PhotonView;
            
            NickName = new SyncVar<string>(nameof(NickName), _photonView);

            LocalNickName = "Player#" + Random.RandomRange(100, 999);
        }   

        private void Update() 
        {
            if(_photonView.IsMine)
            {
                if (LocalNickName != NickName.GetProperty())
                {
                    NickName.SetProperty(LocalNickName);
                }

                if (_container.gameObject.name != LocalNickName)
                {
                    LocalNickName = _localNickName;
                }
            }
            else
            {
                LocalNickName = NickName.GetProperty();
            }
        }
    }
}