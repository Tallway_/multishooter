using System;
using Photon.Pun;
using UnityEngine;

namespace MultiShooter.PlayerBehavior
{
    public class PlayerInput : MonoBehaviour
    {
        public float HorizontalDirection { get; private set; }
        public float VerticalDirection { get; private set; }
        public float MouseX { get; private set; }
        public float MouseY { get; private set; }
        public bool MouseHolding { get; private set; }
        public Vector3 MouseScreenPosition { get; private set; }

        private PlayerBehaviorContainer _playerBehaviorContainer;
        private PhotonView _photonView;

        private void Awake()
        {
            _playerBehaviorContainer = GetComponentInParent<PlayerBehaviorContainer>();
            _photonView = _playerBehaviorContainer.PhotonView;
            
        }

        private void Update() 
        {
            if(!_photonView.IsMine && PhotonNetwork.IsConnected)
            {
                return; 
            }

            HorizontalDirection = Input.GetAxis("Horizontal");
            VerticalDirection = Input.GetAxis("Vertical");
            MouseX = Input.GetAxis("Mouse X");
            MouseY = Input.GetAxis("Mouse Y");
            MouseHolding = Input.GetMouseButton(0);
            MouseScreenPosition = Input.mousePosition;
        }
    }
}