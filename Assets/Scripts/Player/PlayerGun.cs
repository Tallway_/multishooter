﻿using System;
using MultiShooter.Bullets;
using MultiShooter.Global;
using MultiShooter.Sync;
using Photon.Pun;
using UnityEngine;

namespace MultiShooter.PlayerBehavior
{
    public class PlayerGun : MonoBehaviour
    {
        public bool IsFiring { get; private set; }

        [SerializeField] private Bullet _bulletPrefab;
        [SerializeField] private float _rate;

        private SyncVar<bool> _syncIsFiring;
        private PlayerBehaviorContainer _container;
        private PlayerInput _playerInput;
        private PhotonView _photonView;
        private Timer _shootingRateTimer;
        private Transform _transform;

        private void Awake()
        {
            _container = GetComponentInParent<PlayerBehaviorContainer>();
            _transform = transform;
            _photonView = _container.PhotonView;
            _playerInput = _container.PlayerInput;
            
            _syncIsFiring = new SyncVar<bool>(nameof(IsFiring), _photonView);
        }

        private void Start()
        {
            IsFiring = false;
            _shootingRateTimer = new Timer(_rate);
        }

        private void Update()
        {
            if (_photonView.IsMine)
            {
                IsFiring = _playerInput.MouseHolding;
                
                _syncIsFiring.SetProperty(IsFiring);
            }
            else
            {
                IsFiring = _syncIsFiring.GetProperty();
            }

            if (_shootingRateTimer.IsFinished && IsFiring)
            {
                Bullet bullet = Instantiate(_bulletPrefab, _transform.position, Quaternion.identity);
                bullet.Init(_transform.forward);
                bullet.Launch();
                
                _shootingRateTimer.Reset();
            }
            else
            {
                _shootingRateTimer.Tick(Time.deltaTime);
            }
        }
    }
}