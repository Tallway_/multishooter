using System;
using MultiShooter.Sync;
using Photon.Pun;
using UnityEngine;

namespace MultiShooter.PlayerBehavior
{
    public class PlayerHealth : MonoBehaviour
    {
        public event Action OnPlayerDead;
        public float Health 
        {
            get { return _currentHealth; }
            set
            {
                _currentHealth = value;

                if (_photonView.IsMine || PhotonNetwork.IsMasterClient)
                {
                    _syncHealth.SetProperty(value);
                }
            }
        }

        [SerializeField] private float _healthPoints;

        private PlayerBehaviorContainer _container;
        private PhotonView _photonView;
        private SyncVar<float> _syncHealth;
        private float _currentHealth;

        private void Awake()
        {
            _container = GetComponentInParent<PlayerBehaviorContainer>();
            _photonView = _container.PhotonView;
            _syncHealth = new SyncVar<float>(nameof(_syncHealth), _photonView);
            
            Health = _healthPoints;
        }

        private void Update()
        {
            _currentHealth = _syncHealth.GetProperty();
        }

        public bool ApplyDamage(float value)
        {
            if (!PhotonNetwork.IsMasterClient) return false;
            
            if(value < 0f)
            {
                Debug.Log("(PlayerHealth) Incorrect damage value. Damage must be >= 0.");
                return false;
            }

            Health -= value;

            if(Health <= 0f)
            {
                Health = 0;
               
                OnPlayerDead?.Invoke();
            }

            return true;
        }
    }
}