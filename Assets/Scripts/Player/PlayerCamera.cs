using Photon.Pun;
using UnityEngine;

namespace MultiShooter.PlayerBehavior
{
    public class PlayerCamera : MonoBehaviour 
    {
        private PhotonView _photonView;
        private PlayerBehaviorContainer _container;

        private void Awake() 
        {
            _container = GetComponentInParent<PlayerBehaviorContainer>();
            _photonView = _container.PhotonView;
        }

        private void Start() 
        {
            if(!_photonView.IsMine)
            {
                gameObject.SetActive(false);
            }
        }
    }
}