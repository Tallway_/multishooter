using System;
using System.Numerics;
using ExitGames.Client.Photon;
using ExitGames.Client.Photon.StructWrapping;
using IcarosSDK.Scripts.Utilities;
using JetBrains.Annotations;
using Photon.Pun;
using Photon.Realtime;

namespace MultiShooter.Sync
{
    public class SyncVar<T>
    {
        public PhotonView view;
        public string fieldName;
        public string key;
        public Hashtable customPropertyHashtable = new Hashtable();

        public Func<T> GetValueFunc;

        public Action<T> OnInit;

        public SyncVarMonoBehaviour component;

        public SyncVar(string theFieldName, [CanBeNull] PhotonView theView = null, Func<T> theGetValueFunc = null)
        {
            GetValueFunc = theGetValueFunc;
            view = theView;
            fieldName = theFieldName;
            key = BuildKey();

            if (theView != null && theGetValueFunc != null)
            {
                component = theView.gameObject.AddComponent<SyncVarMonoBehaviour>();
                component.AddUpdateCallback(UpdateValue);
                component.AddPhotonView(view);
            }
        }

        void UpdateValue()
        {
            if (view.gameObject.activeSelf && GetValueFunc != null && view.IsMine)
            {
                SetProperty(GetValueFunc());
                
                component.ForceUpdate();
            }
        }

        public bool IsEmpty => customPropertyHashtable.Count == 0;

        public bool IsInitValue = false;

        public void InitValue(Action<T> OnInit = null)
        {
            this.OnInit = OnInit;

            if (component != null)
            {
                component.SetForceUpdateCallback(() =>
                {
                    if (IsExist())
                    {
                        IsInitValue = true;

                        OnInit?.Invoke(GetProperty());
                    }
                });
            }
            
            if (IsExist() && !IsInitValue)
            {
                IsInitValue = true;

                OnInit?.Invoke(GetProperty());
            }

            TimerManager.Instance.Repeat(() =>
            {
                if (IsExist() && !IsInitValue)
                {
                    IsInitValue = true;
                    
                    OnInit?.Invoke(GetProperty());
                        
                    return false;
                }
                    
                return true;
            }, 1f);
        }

        public void UpdateAfterTime(Func<T> GetValue, Func<bool> IsNeedUpdate, float time)
        {
            TimerManager.Instance.Repeat(() =>
            {
                if (view == null)
                {
                    return false;
                }

                if (IsNeedUpdate())
                {
                    SetProperty(GetValue());
                }

                return true;
            }, time);
        }

        public void SetProperty(T value)
        {
            if (view.IsMine || PhotonNetwork.IsMasterClient)
            {
                customPropertyHashtable[key] = value;

                if (PhotonNetwork.IsConnectedAndReady && PhotonNetwork.CurrentRoom != null)
                {
                    PhotonNetwork.CurrentRoom.SetCustomProperties(customPropertyHashtable);
                }
            }
        }

        public bool IsExist()
        {
            if (PhotonNetwork.CurrentRoom == null)
            {
                return false;
            }

            return PhotonNetwork.CurrentRoom.CustomProperties.ContainsKey(key) &&
                   PhotonNetwork.CurrentRoom.CustomProperties[key] != null;
        }

        public T GetProperty()
        {
            if (!IsExist())
            {
                return default(T);
            }

            return (T) PhotonNetwork.CurrentRoom.CustomProperties[key];
        }

        public void RemoveProperty()
        {
            if (PhotonNetwork.CurrentRoom == null)
            {
                return;
            }

            PhotonNetwork.CurrentRoom.CustomProperties[key] = null;
        }

        private string BuildKey()
        {
            string resultKey = string.Empty;

            if (view == null)
            {
                resultKey = $"Common.{nameof(T)}.{fieldName}";
                return resultKey;
            }

            resultKey = $"{view.ViewID}.{nameof(T)}.{fieldName}";
            return resultKey;
        }
    }
}