﻿using System;
using System.IO;
using ExitGames.Client.Photon;
using UnityEngine;

namespace MultiShooter.Sync
{
    internal static class CustomTypes
    {
        internal static bool IsRegistered = false;
        
        internal static void Register()
        {
            if (IsRegistered)
            {
                return;
            }
            
            PhotonPeer.RegisterType(typeof(Color), (byte) 'C', ColorSerialization, ColorDeserialization);
            
            IsRegistered = true;
        }

        #region Serialization/Deserialization methods

        #region Color
        private static byte[] ColorSerialization(object color)
        {
            byte[] bytes = new byte[4]
            {
                Convert.ToByte(((Color) color).r * 255),
                Convert.ToByte(((Color) color).g * 255),
                Convert.ToByte(((Color) color).b * 255),
                Convert.ToByte(((Color) color).a * 255),
            };

            return bytes;
        }

        private static object ColorDeserialization(byte[] bytes)
        {
            Color newColor = new Color(
                    (float)bytes[0] / 255,
                    (float)bytes[1] / 255,
                    (float)bytes[2] / 255,
                    (float)bytes[3] / 255);

            return newColor;
        }
        #endregion

        #endregion
    }

}