﻿using System;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace MultiShooter.Sync
{
    public class SyncVarMonoBehaviour : MonoBehaviourPunCallbacks
    {
        private Action _updateCallback;
        private Action _forceUpdateCallback;
        private PhotonView _photonView;

        public void AddPhotonView(PhotonView photonView)
        {
            _photonView = photonView;
        }
        
        public void AddUpdateCallback(Action updateCallback)
        {
            _updateCallback = updateCallback;
        }

        public void SetForceUpdateCallback(Action setForceCallback)
        {
            _forceUpdateCallback = setForceCallback;
        }

        public void ForceUpdate()
        {
            _photonView.RPC(nameof(ExecuteForceUpdateRPC), RpcTarget.Others);
        }

        [PunRPC]
        public void ExecuteForceUpdateRPC()
        {
            if (!_photonView.IsMine)
            {
                _forceUpdateCallback?.Invoke();
            }
        }
        
        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            if (PhotonNetwork.IsMasterClient && _updateCallback != null)
            {
                _updateCallback?.Invoke();
                
                Invoke("UpdateValue", 0.5f);
                Invoke("UpdateValue", 1f);
                Invoke("UpdateValue", 1.5f);
            }
        }

        public void UpdateValue()
        {
            if (PhotonNetwork.IsMasterClient)
            {
                _updateCallback?.Invoke();
            }
        }
    }
}